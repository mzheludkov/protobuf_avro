#include <iostream>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <cstring>
#include <unistd.h>


#include <jsoncpp/json/json.h>


#include "measurementinfo.h"
#include "dataset.h"
#include "result.h"
#include "protobuf/measurements.pb.h"
#include "avro/measurements.h"

//#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <numeric>

using namespace std;
using boost::asio::ip::tcp;

int readAndDecodeMessageSize(tcp::iostream& stream) {
    int messageSize = 0;
    stream.read(reinterpret_cast<char*>(&messageSize), sizeof(int));

    return messageSize;
}

void processJSON(tcp::iostream& stream){
    Json::Value val;
    Json::Reader reader;

    std::vector<Dataset> datasets;
    std::vector<Result> results;

    /* Read json string from the stream */
    string s;
    getline(stream, s, '\0');

    /* Parse string */
    reader.parse(s, val);

    datasets.clear();
    results.clear();
    for (int i = 0; i < val.size(); i++) {
        datasets.emplace_back();
        datasets[i].Deserialize(val[i]);
        /* Calculate averages */
        results.emplace_back(datasets[i].getInfo(), datasets[i].getRecords());
    }

    /* Create output JSON structure */
    Json::Value out;
//    Json::FastWriter writer;
    Json::StyledWriter writer;
    for (int i = 0; i < results.size(); i++) {
        Json::Value result;
        results[i].Serialize(result);
        out[i] = result;
    }

    /* Send the result back */
    std::string output = writer.write(out);
    stream << output;
    cout << output;
}

void processAvro(tcp::iostream& stream) {
    uint32_t messageSize = readAndDecodeMessageSize(stream);
    if (!messageSize) {
        cout << "Wrong message size";
        return;
    }
//    char *buffer = new char[messageSize];
//    stream.read(buffer, messageSize);

    std::unique_ptr<avro::InputStream> inputStream = avro::istreamInputStream(stream, messageSize);
    esw::ARequest aRequest = {};
    avro::DecoderPtr avroDecoder = avro::binaryDecoder();
    avroDecoder->init(*inputStream);
    avro::decode(*avroDecoder, aRequest);

    esw::AResponse aResponse = {};
    for (esw::ADataset& aDataset: aRequest.datasets) {
        esw::AResult aResult = {};
        aResult.info = aDataset.info;

        aResult.averages.DOWNLOAD = std::accumulate(
                aDataset.records.DOWNLOAD.begin(), aDataset.records.DOWNLOAD.end(), 0.0
        ) / (double)aDataset.records.DOWNLOAD.size();
        aResult.averages.UPLOAD = std::accumulate(
                aDataset.records.UPLOAD.begin(), aDataset.records.UPLOAD.end(), 0.0
        ) / (double)aDataset.records.UPLOAD.size();
        aResult.averages.PING = std::accumulate(
                aDataset.records.PING.begin(), aDataset.records.PING.end(), 0.0
        ) / (double)aDataset.records.PING.size();

        aResponse.results.push_back(std::move(aResult));
    }

    std::unique_ptr<avro::OutputStream> outputStream = avro::ostreamOutputStream(stream);
    avro::EncoderPtr avroEncoder = avro::binaryEncoder();
    avroEncoder->init(*outputStream);
    avro::encode(*avroEncoder, aResponse);
    avroEncoder->flush();
}

void processProtobuf(tcp::iostream& stream) {
    esw::PRequest request;

    int messageSize = readAndDecodeMessageSize(stream);
    char *buffer = new char[messageSize];
    stream.read(buffer, messageSize);
    if (!request.ParseFromArray((void *) buffer, messageSize)) {
        throw std::runtime_error("Error parsing Protobuf request");
    }
    delete[] buffer;

    esw::PResponse response;
    for (esw::PDataSet& dataset : *request.mutable_datasets()) {
        esw::PResult* result = response.mutable_results()->Add();
        *(result->mutable_info()) = std::move(*dataset.mutable_info());

        double downloadAverage = std::accumulate(
                dataset.mutable_records()->download().begin(),
                dataset.mutable_records()->download().end(),
                0.0
        ) / dataset.mutable_records()->download().size();
        result->mutable_averages()->set_download(downloadAverage);

        double uploadAverage = std::accumulate(
                dataset.mutable_records()->upload().begin(),
                dataset.mutable_records()->upload().end(),
                0.0
        ) / dataset.mutable_records()->upload().size();
        result->mutable_averages()->set_upload(uploadAverage);

        double pingAverage = std::accumulate(
                dataset.mutable_records()->ping().begin(),
                dataset.mutable_records()->ping().end(),
                0.0
        ) / dataset.mutable_records()->ping().size();
        result->mutable_averages()->set_ping(pingAverage);
    }
    response.SerializeToOstream(&stream);
}

int main(int argc, char *argv[]) {

    if (argc != 3) {
        cout << "Error: two arguments required - ./server  <port> <protocol>" << endl;
        return 1;
    }



    // unsigned short int port = 12345;
    unsigned short int port = atoi(argv[1]);

    // std::string protocol = "json";
    std::string protocol(argv[2]);
    try {
        boost::asio::io_service io_service;

        tcp::endpoint endpoint(tcp::v4(), port);
        tcp::acceptor acceptor(io_service, endpoint);

        while (true) {
            tcp::iostream stream;
            boost::system::error_code ec;
            acceptor.accept(*stream.rdbuf(), ec);

            if(protocol == "json"){
                processJSON(stream);
            }else if(protocol == "avro"){
                processAvro(stream);
            }else if(protocol == "proto"){
                processProtobuf(stream);
            }else{
                throw std::logic_error("Protocol not yet implemented");
            }

        }

    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
