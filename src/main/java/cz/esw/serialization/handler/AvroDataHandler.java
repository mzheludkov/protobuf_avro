package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.avro.*;
import cz.esw.serialization.json.DataType;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.commons.lang3.ArrayUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Marek Cuchý (CVUT)
 */
public class AvroDataHandler implements DataHandler {
	private final InputStream is;
	private final OutputStream os;
	protected Map<Integer, ADataset> datasets;

	public AvroDataHandler(InputStream is, OutputStream os) {
		this.is = is;
		this.os = os;
	}

	@Override
	public void initialize() {
		datasets = new HashMap<>();
	}

	@Override
	public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
		ADataset aDataset = new ADataset();
		AMeasurementInfo aMeasurementInfo = new AMeasurementInfo();
		aMeasurementInfo.setId(datasetId);
		aMeasurementInfo.setTimestamp(timestamp);
		aMeasurementInfo.setMeasurerName(measurerName);
		aDataset.setInfo(aMeasurementInfo);
		datasets.put(datasetId, aDataset);
	}

	@Override
	public void handleValue(int datasetId, DataType type, double value) {
		ADataset aDataset = datasets.get(datasetId);
		if (aDataset == null) {
			throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
		}
		ADataRecords aDataRecords = aDataset.getRecords();
		if (aDataRecords == null) {
			aDataRecords = new ADataRecords();
			aDataRecords.setDOWNLOAD(new ArrayList<>());
			aDataRecords.setUPLOAD(new ArrayList<>());
			aDataRecords.setPING(new ArrayList<>());
		}
		switch (type) {
			case DOWNLOAD -> aDataRecords.getDOWNLOAD().add(value);
			case UPLOAD -> aDataRecords.getUPLOAD().add(value);
			case PING -> aDataRecords.getPING().add(value);
		}
		aDataset.setRecords(aDataRecords);
	}

	@Override
	public void getResults(ResultConsumer consumer) throws IOException {
		ARequest aRequest = new ARequest();
		aRequest.setDatasets(new ArrayList<>());
		for (ADataset aDataset: datasets.values()) {
			aRequest.getDatasets().add(aDataset);
		}

		DatumWriter<ARequest> aRequestWriter = new SpecificDatumWriter<>(ARequest.class);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(byteArrayOutputStream, null);
		aRequestWriter.write(aRequest, encoder);
		encoder.flush();

		int messageSize = byteArrayOutputStream.size();
		byte[] size = ByteBuffer.allocate(4)
				.putInt(messageSize)
				.array();
		ArrayUtils.reverse(size);
		os.write(size);
		os.write(byteArrayOutputStream.toByteArray());

		DatumReader<AResponse> aResponseReader = new SpecificDatumReader<>(AResponse.class);
		BinaryDecoder decoder = DecoderFactory.get().binaryDecoder(is, null);
		AResponse aResponse = aResponseReader.read(null, decoder);

		for (AResult result: aResponse.getResults()) {
			AMeasurementInfo aMeasurementInfo = result.getInfo();
			consumer.acceptMeasurementInfo(
					aMeasurementInfo.getId(),
					aMeasurementInfo.getTimestamp(),
					aMeasurementInfo.getMeasurerName().toString()
			);
			ADataAverages dataAverages = result.getAverages();
			consumer.acceptResult(DataType.DOWNLOAD, dataAverages.getDOWNLOAD());
			consumer.acceptResult(DataType.PING, dataAverages.getPING());
			consumer.acceptResult(DataType.UPLOAD, dataAverages.getUPLOAD());
		}
	}
}
