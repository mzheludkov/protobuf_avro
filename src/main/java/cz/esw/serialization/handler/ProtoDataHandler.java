package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.json.DataType;
import cz.esw.serialization.proto.*;
import org.apache.commons.lang3.ArrayUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Marek Cuchý (CVUT)
 */
public class ProtoDataHandler implements DataHandler {
	protected Map<Integer, PDataSet> datasets;
	private final InputStream is;
	private final OutputStream os;

	public ProtoDataHandler(InputStream is, OutputStream os) {
		this.is = is;
		this.os = os;
	}

	@Override
	public void initialize() {
		datasets = new HashMap<>();
	}

	@Override
	public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
		PDataSet dataset = PDataSet.newBuilder()
				.setInfo(
						PMeasurementInfo.newBuilder()
								.setId(datasetId)
								.setMeasurerName(measurerName)
								.setTimestamp(timestamp)
				)
				.build();
		datasets.put(datasetId, dataset);
	}

	@Override
	public void handleValue(int datasetId, DataType type, double value) {
		PDataSet dataset = datasets.get(datasetId);
		if (dataset == null) {
			throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
		}
		PDataSet.PDataRecords pDataRecords = dataset.getRecords().toBuilder()
				.addRepeatedField(
						PDataSet.PDataRecords.getDescriptor().findFieldByName(type.name()),
						value
				).build();
		PDataSet newValue = dataset.toBuilder().setRecords(
			pDataRecords
		)
			.build();
		datasets.put(datasetId, newValue);
	}

	@Override
	public void getResults(ResultConsumer consumer) throws IOException {
		PRequest.Builder pRequestBuilder = PRequest.newBuilder();
		for (Map.Entry<Integer, PDataSet> dataset: this.datasets.entrySet()) {
			pRequestBuilder.addDatasets(dataset.getValue().toBuilder().build());
		}
		PRequest pRequest = pRequestBuilder.build();
		int length = pRequest.getSerializedSize();
		byte[] size = ByteBuffer.allocate(4)
				.putInt(length)
				.array();
		ArrayUtils.reverse(size);
		os.write(size);
		pRequest.writeTo(os);

		PResponse pResponse = PResponse.parseFrom(is);

		for (PResult pResult: pResponse.getResultsList()) {
			PMeasurementInfo pMeasurementInfo = pResult.getInfo();
			consumer.acceptMeasurementInfo(
					pMeasurementInfo.getId(),
					pMeasurementInfo.getTimestamp(),
					pMeasurementInfo.getMeasurerName()
			);

			PResult.PAverages dataAverages = pResult.getAverages();

			consumer.acceptResult(DataType.DOWNLOAD, dataAverages.getDownload());
			consumer.acceptResult(DataType.UPLOAD, dataAverages.getUpload());
			consumer.acceptResult(DataType.PING, dataAverages.getPing());
		}
	}
}
