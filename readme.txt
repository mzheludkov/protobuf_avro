Je vidět, že Protobuf a Avro asi tak 5-6 krát rychlejší, než používání JSON formátu. Možná někde mám ne úplně efektivní kód, myslím si, že dalo by se ty manipulací s datami ještě nějak zlepšit.
Pro generování tříd z .proto a .avsc jsem používal "protoc --cpp_out=. measurements.proto" a "avrogencpp -i measurements.avsc -o measurements.h -n esw"

JSON
Time needed to send and receive data: 500ms
Total time: 592ms
Data are correct.
Time needed to send and receive data: 306ms
Total time: 328ms
Data are correct.

Protobuf
Time needed to send and receive data: 102ms
Total time: 861ms
Data are correct.
Time needed to send and receive data: 79ms
Total time: 441ms
Data are correct.

Avro
Time needed to send and receive data: 103ms
Total time: 241ms
Data are correct.
Time needed to send and receive data: 46ms
Total time: 64ms
Data are correct.